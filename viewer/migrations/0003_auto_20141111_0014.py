# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0002_auto_20141110_1155'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookshelf',
            name='image',
            field=models.ImageField(upload_to=b'unit'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rawimage',
            name='image',
            field=models.ImageField(upload_to=b'raw'),
            preserve_default=True,
        ),
    ]
