from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from nakedlib.settings import MEDIA_URL, MEDIA_ROOT

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'nakedlib.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^viewer/', include('viewer.urls')),
) + static(MEDIA_URL, document_root=MEDIA_ROOT)
