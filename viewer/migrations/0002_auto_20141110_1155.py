# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('viewer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookshelf',
            name='image',
            field=models.ImageField(upload_to=b'/static/unit'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='rawimage',
            name='image',
            field=models.ImageField(upload_to=b'/static/raw'),
            preserve_default=True,
        ),
    ]
