from django.db import models
import os
from nakedlib.settings import STATIC_URL

class RawImage(models.Model):
    location = models.IntegerField()
    level = models.IntegerField()
    side = models.IntegerField()
    image = models.ImageField(upload_to="raw")
    created = models.DateTimeField(auto_now=True)

class Bookshelf(models.Model):
    rackId = models.IntegerField()
    row = models.IntegerField()
    column = models.IntegerField()
    side = models.IntegerField()
    image = models.ImageField(upload_to="unit")
    created = models.DateTimeField(auto_now=True)
