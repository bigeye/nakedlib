from django.shortcuts import render
from django.forms import ModelForm
from viewer.models import RawImage
from django.shortcuts import render
from django.http import HttpResponse

class RawImageForm(ModelForm):
    class Meta:
        model = RawImage
        field = ['location', 'level', 'side', 'image']

def index(request):
    return render(request, 'viewer.html', {})

def add_raw_image(request):
    args = {'request': request, 'raw_images': RawImage.objects.all()}
    if request.method == 'POST':
        print request.POST
        print request.FILES
        form = RawImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            form = RawImageForm()
            args['complete'] = True
    else:
        form = RawImageForm()
    args['form'] = form
    return render(request, 'viewer_raw.html', args)
